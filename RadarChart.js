import React, { Component } from "react";
import { Text as ReactText, View, Image as ReactImage } from "react-native";
import Svg, {
    G,
    Text,
    Path,
    Line,
} from 'react-native-svg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Radar from "paths-js/radar";
import images from './assets'

import { Options, identity, styleSvg, fontAdapt, midPoint } from "../helpers/util";

function accessKeys(keys) {
    let a = {};
    for (let i in keys) {
        let key = keys[i];
        a[key] = identity(key);
    }
    return a;
}

export default class RadarChart extends Component {
    static defaultProps = {
        options: {
            width: 600,
            height: 600,
            margin: { top: 20, left: 20, right: 20, bottom: 20 },
            r: 300,
            max: 150,
            rings: 3,
            fill: "#2980B9",
            stroke: "#2980B9",
            animate: {
                type: "oneByOne",
                duration: 200,
                fillTransition: 3
            },
            label: {
                fontFamily: "Arial",
                fontSize: 14,
                bold: true,
                color: "#34495E"
            }
        }
    };

    render() {
        const noDataMsg = this.props.noDataMessage || "No data available";
        if (this.props.data === undefined)
            return <ReactText>{noDataMsg}</ReactText>;

        const options = new Options(this.props);

        const x = options.chartWidth / 2;
        const y = options.chartHeight / 2;
        const radius = Math.min(x, y);

        const center = this.props.center || [x, y];

        const keys = Object.keys(this.props.data[0]);
        const keys_value = this.props.data[0];
        const chart = Radar({
            center: this.props.center || [x, y],
            r: this.props.options.r || radius,
            data: this.props.data,
            accessor: this.props.accessor || accessKeys(keys),
            max: this.props.options.max,
            rings: 12
        });
        const self = this;
        const colors = styleSvg({}, self.props.options);
        // const colorsFill = self.props.options.fill;
        // const curves = chart.curves.map(function (c, i) {
        //   const color = colorsFill instanceof Array ? colorsFill[i] : colorsFill;
        //   return (
        //     <Path
        //       key={i}
        //       d={c.polygon.path.print()}
        //       fill={color}
        //       fillOpacity={0.6}
        //     />
        //   );
        // });

        const length = chart.rings.length;
        const rings = chart.rings.map(function (r, i) {
            let fill = (i % 2 == 0) ? 'rgb(226, 228, 234)' : 'rgb(205, 210, 220)';
            console.log('fill', fill)
            if (i !== length - 1) return (
                <Path
                    key={"rings" + i}
                    d={r.path.print()}
                    stroke={fill}
                    strokeOpacity={colors.strokeOpacity}
                    //  strokeDasharray={[1,3,6]}
                    strokeWidth={13}
                    fill={'none'}
                    fillRulec="evenodd"
                />
            );
        });

        const textStyle = fontAdapt(options.label);
        const divederLine = chart.rings[length - 1].path.points().map(function (p, i) {
            var strokeWidth = (i % 3 == 0 ? true : false)
            if (i !== length - 1) {
                return (
                    <G key={"line" + i}>
                        <Line
                            x1={p[0]}
                            y1={p[1]}
                            x2={center[0]}
                            y2={center[1]}
                            stroke={'white'}
                            strokeOpacity={colors.strokeOpacity}
                            strokeDasharray={strokeWidth ? null : [10, 6]}
                            strokeWidth={strokeWidth ? 10 : 1}
                        />
                    </G>
                );
            }
        });

        const labels = chart.rings[length - 1].path.rotate(20, center[0], center[1]).points().map(function (p, i) {
            return (
                <G key={"label" + i}>
                    <Text
                        fontFamily={textStyle.fontFamily}
                        fontSize={textStyle.fontSize}
                        fontWeight={textStyle.fontWeight}
                        fontStyle={textStyle.fontStyle}
                        fill={'rgb(205, 210, 220)'}
                        //onPress={onLabelPress}
                        textAnchor="middle"
                        x={Math.floor(p[0])}
                        y={Math.floor(p[1])}

                    >
                        {keys[i]}
                    </Text>

                </G>
            );
        });



        const profilePlotter = this.props.meberList.map((member, i) => {
            if (!member.type || !member.number) {
                return null;
            }
            let index = keys.indexOf(member.type);
            let p = chart.rings[member.number - 2].path.rotate(member.angle, center[0], center[1]).points()[index]
            let p1 = chart.rings[member.number - 1].path.rotate(member.angle, center[0], center[1]).points()[index]
            p[0] = midPoint(p1[0], p[0])
            p[1] = midPoint(p1[1], p[1])
            let profileImage = member.profileImageUrl === 'Not Specified' ? images.defaultProfileImage : { uri: member.profileImageUrl }

            return (
                <G key={"label" + i} >
                    <View style={{ position: 'absolute', left: Math.floor(p[0]) - 7, top: Math.floor(p[1]) - 7 }}>
                        <ReactImage style={{ width: hp(2), height: hp(2), borderRadius: hp(2) / 2, borderColor: 'white', borderWidth: 1.5 }} source={profileImage} />
                    </View>
                    {/* <Image
            x={Math.floor(p[0]) - 7}
            y={Math.floor(p[1]) - 7}
            stroke={'white'}
            strokeWidth={2}
            strokeOpacity={1}
            width={15}
            height={15}
            preserveAspectRatio="xMidYMid slice"
            opacity="1"
            href={profileImage}
          /> */}
                </G>
            );
        });

        return (
            <Svg width={options.width} height={options.height}>
                <G x={options.margin.left} y={options.margin.top}>
                    <G x={options.margin.left * -1} y={options.margin.top * -1}>
                        {rings}
                        {profilePlotter}
                        {labels}
                        {divederLine}
                    </G>
                </G>
            </Svg>
        );
    }
}