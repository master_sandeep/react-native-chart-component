import React, { Component } from 'react'
import { Text as ReactText, Animated } from 'react-native'
import Svg, { G, Path, Text, Circle, Rect, Polygon } from 'react-native-svg'
import { Colors, Options, cyclic, identity, fontAdapt } from "../helpers/util";
import _ from 'lodash'
const Pie = require('paths-js/pie')


export default class PieChart extends Component {

    static defaultProps = {
        options: {
            margin: { top: 20, left: 20, right: 20, bottom: 20 },
            width: 600,
            height: 600,
            color: '#2980B9',
            r: 100,
            R: 200,
            legendPosition: 'topLeft',
            animate: {
                enabled: false,
                type: 'oneByOne',
                duration: 200,
                fillTransition: 3
            },
            label: {
                fontFamily: 'Arial',
                fontSize: 14,
                bold: true,
                color: '#ECF0F1'
            }
        },
    }

    constructor(props) {
        super(props);

        this._animationRefArray = {};
        if (this._shouldAnim()) this._animationArray = new Array(props.data.length)

        this.state = { selectedIndex: undefined }

    }

    _shouldAnim = () => this.props.options.animate.enabled && this.props.data.length > 1

    color(i) {
        let color = this.props.color || (this.props.options && this.props.options.color)
        if (Array.isArray(color)) {
            if (i >= color.length) {
                const pallete = Colors.mix(color[i % color.length])
                return Colors.string(cyclic(pallete, i))
            }
            return color[i];
        } else {
            if (color && !_.isString(color)) color = color.color
            let pallete = this.props.pallete || (this.props.options && this.props.options.pallete) || Colors.mix(color || '#9ac7f7')
            return Colors.string(cyclic(pallete, i))
        }
    }

    get defaultRange() {
        return _.map(Array(this.props.data && this.props.data.length), function () { return 0 })
    }

   
    render() {
        const noDataMsg = this.props.noDataMessage || 'No data available'
        if (this.props.data === undefined) return (<ReactText>{noDataMsg}</ReactText>)

        let options = new Options(this.props)

        let x = (options.chartWidth / 2) - (options.margin.left || 0)
        let y = (options.chartHeight / 2) - (options.margin.top || 0)

        let radius = Math.min(x, y)

        let r = this.props.r
        r = (isNaN(r) ? (this.props.options && this.props.options.r) : r)
        r = (isNaN(r) ? (radius / 2) : r)

        let R = this.props.R
        R = (R || (this.props.options && this.props.options.R))
        R = (R || radius)

        let [centerX, centerY] = this.props.center || (this.props.options && this.props.options.center) || [x, y]

        let textStyle = fontAdapt(options.label)

        let slices

        if (this.props.data.length === 1) {
            let item = this.props.data[0]
            let outerFill = (item.color && Colors.string(item.color)) || this.color(0)
            let innerFill = this.props.monoItemInnerFillColor || '#fff'
            let stroke = typeof fill === 'string' ? outerFill : Colors.darkenColor(outerFill)
            slices = (
                <G>
                    <Circle r={R} cx={centerX} cy={centerY} stroke={stroke} fill={outerFill} />
                    <Circle r={r} cx={centerX} cy={centerY} stroke={stroke} fill={innerFill} />
                    <Text fontFamily={textStyle.fontFamily}
                        fontSize={textStyle.fontSize}
                        fontWeight={textStyle.fontWeight}
                        fontStyle={textStyle.fontStyle}
                        fill={textStyle.fill}
                        textAnchor="middle"
                        x={centerX}
                        y={centerY - R + ((R - r) / 2)}>{item.name}</Text>
                </G>
            )
        } else {
            let chart = Pie({
                center: [centerX, centerY],
                r,
                R,
                data: this.props.data,
                accessor: this.props.accessor || identity(this.props.accessorKey)
            })
            let R1 = R - 70;
            let chart2 = Pie({
                center: [centerX, centerY],
                r: R,
                R: R1,
                data: this.props.data,
                accessor: this.props.accessor || identity(this.props.accessorKey)
            })

            slices = chart.curves.map((c, i) => {
                let fill = (c.item.color && Colors.string(c.item.color)) || this.color(i)
                let stroke = typeof fill === 'string' ? fill : Colors.darkenColor(fill)
                const opacity = this._shouldAnim() ? 0 : 1
                const finalStroke = this._shouldAnim() ? undefined : stroke

                return (

                    <G key={i}>
                        <Path onPress={() => { this.setState({ selectedIndex: i }) }} ref={ref => (this._animationRefArray[`SLICE${i}`] = ref)} fill={fill} fillOpacity={opacity} d={c.sector.path.print()} stroke={finalStroke} />
                        <G x={options.margin.left} y={options.margin.top}>
                            {c.item[this.props.accessorKey] !== 0 && <Text fontFamily={textStyle.fontFamily}
                                fontSize={textStyle.fontSize}
                                fontWeight={textStyle.fontWeight}
                                fontStyle={textStyle.fontStyle}
                                fill={textStyle.fill}
                                textAnchor="middle"
                                x={chart2.curves[i].sector.centroid[0] - 10}
                                y={chart2.curves[i].sector.centroid[1]}>{c.item[this.props.accessorKey] + '%'}</Text>}
                        </G>
                    </G>
                )
            })
        }

        let highlightSlice;
        if (this.state.selectedIndex !== undefined) {
            let R1 = R + 10;
            let chart = Pie({
                center: [centerX, centerY],
                r: R,
                R: R1,
                data: this.props.data,
                accessor: this.props.accessor || identity(this.props.accessorKey)
            })

            let c = chart.curves[this.state.selectedIndex];
            let fill = (c.item.color && Colors.string(c.item.color)) || this.color(this.state.selectedIndex)
            const opacity = .5;

            highlightSlice = (<G key={'selected'}>
                <Path fill={fill} fillOpacity={opacity} d={c.sector.path.print()} />
                <Rect
                    x={c.sector.centroid[0] - 35}
                    y={c.sector.centroid[1] - 50}
                    fill="white"
                    stroke="rgba(0, 0, 0, 0.16)"
                    strokeWidth="1"
                    width="70"
                    height="40"
                />
                <Text fontFamily={textStyle.fontFamily}
                    fontSize={6}
                    fontStyle={textStyle.fontStyle}
                    fill={'black'}
                    textAnchor="middle"
                    x={c.sector.centroid[0]}
                    inlineSize={1}
                    y={c.sector.centroid[1] - 35}>{c.item.name}</Text>
                <Text fontFamily={textStyle.fontFamily}
                    fontSize={textStyle.fontSize}
                    fontWeight={'bold'}
                    fontStyle={textStyle.fontStyle}
                    fill={'black'}
                    textAnchor="middle"
                    x={c.sector.centroid[0] - 10}
                    y={c.sector.centroid[1] - 20}>{c.item[this.props.accessorKey]}%</Text>
                <Polygon
                    points={`${c.sector.centroid[0] - 10},${c.sector.centroid[1] - 10} ${c.sector.centroid[0] + 10},${c.sector.centroid[1] - 10} ${c.sector.centroid[0]},${c.sector.centroid[1]}`}
                    fill="white"
                    stroke="rgba(0, 0, 0, 0.16)"
                    strokeWidth="1"
                />
            </G>
            )
        }

        let returnValue = <Svg width={options.width} height={options.height} >
            <G x={20} y={24}>
                {slices}
                {highlightSlice}
            </G>
        </Svg>

        return returnValue
    }
}
